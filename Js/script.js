$(document).ready(function () {
  $("body").on("load", function () {
    $.ajax({
      type: "GET",
      url: "http://localhost:3000/posts",
      dataType: "json",
      async: true,
      success: function (data) {
        console.log(data);
        let products = "";
        let table="";
        $.each(data, function (i, v) {
            table+=` <tr> 
            <td>${v.id}</td>
            <td>${v.author}</td>
            <td>${v.title.substring(0, 10)}</td>
          </tr>`
          products += `
          <div class="product">
            <h4>${v.id}</h4>
            <h3>${v.author}</h3>
            <h2>${v.title.substring(0, 10)}</h2>
          </div>
          `;
        });
        $(".product-container").append(products);
        $("#table1").append(table);
        $("#waiting").hide();
      },
      error: function () {
        console.log("not able to process request");
      },
    });
  });
  $("body").trigger("load");

  $("#searchkey").on("keyup", function () {
    let key = $(this).val().toLowerCase();

    $(".product").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(key) > -1);
    });
  });
});
